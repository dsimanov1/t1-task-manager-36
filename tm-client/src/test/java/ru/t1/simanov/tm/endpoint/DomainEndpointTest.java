package ru.t1.simanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.simanov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.simanov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.simanov.tm.api.service.IPropertyService;
import ru.t1.simanov.tm.dto.request.*;
import ru.t1.simanov.tm.marker.IntegrationCategory;
import ru.t1.simanov.tm.service.PropertyService;

@Category(IntegrationCategory.class)
public final class DomainEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final IDomainEndpoint domainEndpointClient = IDomainEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(propertyService.getAdminLogin());
        loginRequest.setPassword(propertyService.getAdminPassword());
        adminToken = authEndpointClient.loginUser(loginRequest).getToken();
    }

    @Test
    public void backupLoadData() {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.backupLoadData(request));
    }

    @Test
    public void backupSaveData() {
        @NotNull final DataBackupSaveRequest request = new DataBackupSaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.backupSaveData(request));
    }

    @Test
    public void base64LoadData() {
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.base64LoadData(request));
    }

    @Test
    public void base64SaveData() {
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.base64SaveData(request));
    }

    @Test
    public void binaryLoadData() {
        @NotNull final DataBinaryLoadRequest request = new DataBinaryLoadRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.binaryLoadData(request));
    }

    @Test
    public void binarySaveData() {
        @NotNull final DataBinarySaveRequest request = new DataBinarySaveRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.binarySaveData(request));
    }

    @Test
    public void jsonLoadFasterXmlData() {
        @NotNull final DataJsonLoadFasterXmlRequest request = new DataJsonLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.jsonLoadFasterXmlData(request));
    }

    @Test
    public void jsonSaveFasterXmlData() {
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.jsonSaveFasterXmlData(request));
    }

    @Test
    public void jsonLoadJaxBData() {
        @NotNull final DataJsonLoadJaxBRequest request = new DataJsonLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.jsonLoadJaxBData(request));
    }

    @Test
    public void jsonSaveJaxBData() {
        @NotNull final DataJsonSaveJaxBRequest request = new DataJsonSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.jsonSaveJaxBData(request));
    }

    @Test
    public void xmlLoadFasterXmlData() {
        @NotNull final DataXmlLoadFasterXmlRequest request = new DataXmlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.xmlLoadFasterXmlData(request));
    }

    @Test
    public void xmlSaveFasterXmlData() {
        @NotNull final DataXmlSaveFasterXmlRequest request = new DataXmlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.xmlSaveFasterXmlData(request));
    }

    @Test
    public void xmlLoadJaxBData() {
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.xmlLoadJaxBData(request));
    }

    @Test
    public void xmlSaveJaxBData() {
        @NotNull final DataXmlSaveJaxBRequest request = new DataXmlSaveJaxBRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.xmlSaveJaxBData(request));
    }

    @Test
    public void yamlLoadFasterXmlData() {
        @NotNull final DataYamlLoadFasterXmlRequest request = new DataYamlLoadFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.yamlLoadFasterXmlData(request));
    }

    @Test
    public void yamlSaveFasterXmlData() {
        @NotNull final DataYamlSaveFasterXmlRequest request = new DataYamlSaveFasterXmlRequest(adminToken);
        Assert.assertNotNull(domainEndpointClient.yamlSaveFasterXmlData(request));
    }

}
