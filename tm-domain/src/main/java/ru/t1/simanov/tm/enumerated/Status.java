package ru.t1.simanov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@NotNull final Status status) {
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@NotNull final String value) {
        if (value.isEmpty()) return null;
        for (@Nullable final Status status : values()) {
            assert status != null;
            if (status.name().equals(value)) return status;
        }
        return null;
    }

}
